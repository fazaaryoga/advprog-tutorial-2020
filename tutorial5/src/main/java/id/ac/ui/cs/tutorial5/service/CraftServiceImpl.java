package id.ac.ui.cs.tutorial5.service;

import id.ac.ui.cs.tutorial5.core.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class CraftServiceImpl implements CraftService {

    private String[] itemName = {
            "Dragon Breath", "Void Rhapsody", "Determination Symphony",
            "Opera of Wasteland", "FIRE BIRD"
    };

    private  List<CraftItem> allItems = new ArrayList<>();

    @Override
    public CraftItem createItem(String itemName) {
        CraftItem craftItem;
        switch (itemName) {
            case "Dragon Breath":
                craftItem = createDragonBreath();
                break;
            case "Void Rhapsody":
                craftItem = createVoidRhapsody();
                break;
            case "Determination Symphony":
                craftItem = createDeterminationSymphony();
                break;
            case "Opera of Wasteland":
                craftItem = createOperaOfWasteland();
                break;
            case "FIRE BIRD":
                craftItem = createFireBird();
                break;
            default:
                craftItem = createFireBird();
        }
        allItems.add(craftItem);
        return craftItem;
    }

    // @TODO convert all creational method to async using CompleteableFuture
    // @TODO Based on Design principe DRY, fix code duplication from running async
    private CraftItem createDragonBreath() {
        CraftItem dragonBreath = new CraftItem("Dragon Breath");
        CompletableFuture.runAsync(() ->
            dragonBreath.addRecipes(new SilentField())
        ).thenRunAsync(() ->
            dragonBreath.addRecipes(new FireCrystal())
        ).thenRun(() -> dragonBreath.composeRecipes());
        return dragonBreath;
    }

    private CraftItem createVoidRhapsody() {
        CraftItem voidRhapsody = new CraftItem("Void Rhapsody");
        CompletableFuture.runAsync(() ->
            voidRhapsody.addRecipes(new SilentField())
        ).thenRunAsync(() ->
        voidRhapsody.addRecipes(new PureWaterfall())
        ).thenRun(() ->
            voidRhapsody.composeRecipes()
        );
        return voidRhapsody;
    }

    private CraftItem createDeterminationSymphony() {
        CraftItem determinationSymphony = new CraftItem("Determination Symphony");
        CompletableFuture.runAsync(() ->
            determinationSymphony.addRecipes(new BlueRoseRainfall())
        ).thenRunAsync(() ->
            determinationSymphony.addRecipes(new PureWaterfall())
        ).thenRun(() ->
            determinationSymphony.composeRecipes()
        );

        return determinationSymphony;
    }

    private CraftItem createOperaOfWasteland() {
        CraftItem wasteland = new CraftItem("Opera of Wasteland");
        CompletableFuture.runAsync(() ->
            wasteland.addRecipes(new DeathSword())
        ).thenRunAsync(()->
            wasteland.addRecipes(new SilentField())
        ).thenRunAsync(() ->
            wasteland.addRecipes(new FireCrystal())
        ).thenRun(() ->
            wasteland.composeRecipes()
        );
        return wasteland;
    }

    private CraftItem createFireBird() {
        CraftItem fireBird =  new CraftItem("FIRE BIRD");
        CompletableFuture.runAsync(() -> 
            fireBird.addRecipes(new FireCrystal())
        ).thenRunAsync(()->
            fireBird.addRecipes(new BirdEggs())
        ).thenRun(() ->
            fireBird.composeRecipes()
        );
        return fireBird;
    }

    @Override
    public String[] getItemNames() {
        return itemName;
    }

    @Override
    public List<CraftItem> findAll() {
        return allItems;
    }
}
