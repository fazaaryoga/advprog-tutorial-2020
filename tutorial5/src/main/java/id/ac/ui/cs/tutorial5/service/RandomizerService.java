package id.ac.ui.cs.tutorial5.service;

import java.util.Random;

public class RandomizerService {

    public static long getRandomCostValue() {
        return getRandomNumberInRange(1000,10000);
    }

    private static int getRandomNumberInRange(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
 }
