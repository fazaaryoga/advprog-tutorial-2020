In this tutorial we asked to implement asynchronous programming to the given template code. One of
the classes we can use to do this is CompleteableFuture. CompleteableFuture supplies various methods
we can use to run our processes in different threads. I used the runAsync, thenRunAsync, and thenRun 
methods to implement asynchronous programming. I first call the CompleteAble.runAsync() method to
run the first void creational method to run said method in a background thread, then i append the 
thenRunAsync() method to consecutive void methods. After all void methods are run in background thread i added the 
thenRun() method at the of the asynchronous code block to make the messages are displayed correctly