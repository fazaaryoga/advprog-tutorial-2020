# Tutorial 5 :  We Can't Afford to Spend Too Much Time For Automatic Crafting So I Make It Asynchronous
If there's anything called the magical wonder of this magical world, it must be the place you are right now. A seventy five thousands stories building that stands proudly above the ground and pierces the very sky where the sun and clouds reside. Before your eyes, the sight of life is presented. You could see the sun and clouds below you. 

While you spend your time enjoying the great spectacle offered to you, you can't help but think about The Assistant. You spent your time together only for a short time, yet The Assistant is one of the few whose company is enjoyable to you. You think that whatever happens, you will reunite with The Assistant and spend time together again, whenever it will happen.

The sun is set. You don't realize that you've spent hours doing sightseeing and reminiscing your time with The Assistant. It's a nice and healthy distraction from works, but you still need to do your work as Intern properly, so you decide to return to your working desk.

When you arrive at your desk, The Manager, whom you know as The Recruiter when you meet for the first time, is already waiting for you with another job in hands. 

"So what work will I do this time?"

"It's another 'working magic but poorly executed' type of work. I can explain how the magic works but you might want to see the code for yourself"

Before The Manager give you the code, you feel something strange happens to you. This feeling is similar with feeling you experience when you suddenly remember knowledge you have as programmer in your past life but you don't think this is the case, since you have yet to see the code The Manager's speak of. You think that The Manager's answer is the cause, but you cannot find the reason for it.

The Manager bring the magic for you to look at. First, you learn how the code is structured
```
core
	Craftable.java
	BirdEggs.java
	BlueRoseRainfall.java
	DeathSword.java
	FireCrystal.java
	PureWaterfall.java
	SilentField.java
	CraftItem.java (modification is allowed)
service
	CraftService.java
	CraftServiceImpl.java (modification is allowed)
	RandomizerService.java
CraftController.java
Tutorial5Application.java
```
"So the magic is used to craft various item and combine them into one item, right? So what is the problem?", you ask The Manager

"Try it for yourself"

You can open the magic through `localhost:8080/craft/` .
As you try the magic, you find that the crafting process takes too much time. You ask The Manager but there's nothing you can do to make the crafting process faster, since all of them are powerful items. 

"If I can't cut the cost of crafting the individual items, what am I supposed to do?", you think for yourself with confusion. 

"If The Manager gives you a work where you can't do anything, it must make no sense, right?", you concluded. There must be another way to cut the process cost, but what would that be?

"Of course, the combining process", you said to yourself. You suddenly look into the code, trying to see implementation of combining process. It turns out that the combining process is done sequentially, which means only one item can be crafted at the time.

"So that's why the combining process is unsurprisingly costly"

You find your conclusion, but you still don't know how to solve the problem. You think that crafting multiple items in one time might be a good solution but given the current state of the magic, it can't be done.

Suddenly, you feel pain in your head. You feel a feeling you already get used to for a long time. It's another revelation. You try to focus so you can hear the revelation clearly.

"Asynchronous.... `CompletableFuture`"
## Asynchronous Programming in Java
Of course, Asynchronous programming and `CompletableFuture`. You already know what you need, but since it's already a long time after you use `CompletableFuture`, you want to review it, alongside what asynchronous programming is.

Asynchronous programming is essentially making a program that can do more than one task at given time. In asynchronous programming, one task is executed and another task can be executed while before the previous task is done .. You recall what your lecturer said about asynchronous programming.

"Let us imagine that you want to make a hot dog. First you cook the bread, then you cook the sausage. After both are cooked, you have your hot dog, but you realize that you can make it faster. You cook your bread, then while waiting the bread to be cooked, you cook the sausage. After both are cooked, you have your hot dog. That is essentially how asynchronous programming works", your lecturer said as you recall the lecture. You can't help but giggle a little since the example your lecturer used is very similar to your work right now.

One of many way to make an asynchronous program is by using `CompletableFuture`. `CompletableFuture` is essentially an object that runs an asynchronous task. With `CompletableFuture`, you can know whether the task is already completed, is it cancelled. You can also make sure that a certain line of code is executed after task/tasks is/are completed.

**Note : You can find about `CompletableFuture` [here](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/CompletableFuture.html). Any additional reading about asynchronous programming and `CompletableFuture` is recommended.** 

Now that you have every tool necessary to solve this problem, you make a plan to do it. Your plan is:

1. Make the crafting process of individual items asynchronous
2. Make sure that the combining process is executed after crafting process of every individual item needed is done
3. Bonus : You might not want to leave code smells in your code, so you might want to refactor the code.

Of course, as an intern, you can't be trusted by The Association with confidential program so The Manager **only allows modification to programs marked as "modification is allowed"** (see the code structure)

**Note: You are allowed to use anything other than `CompletableFuture` to do this tutorial as long as you implement asynchronous programming**
